use escargot;
use predicates::prelude::*;
use std::path::{Path, PathBuf};
use tempfile::tempdir;

#[test]
fn test_direct() {
    let tmpdir = tempdir().unwrap();
    let out_file = tmpdir.path().join("out_test.fa");
    let args = [
        "--acc_file",
        "tests/data/acc_with_alt.txt",
        "tests/data/test.fa",
        "--out",
        out_file.to_str().unwrap(),
    ];
    let predicate_file = predicates::path::eq_file(Path::new("tests/data/test_direct_outref.fa"))
        .utf8()
        .unwrap();
    let status = escargot::CargoBuild::new()
        .bin("seqbyacc")
        //.current_release()
        //.current_target()
        .run()
        .unwrap()
        .command()
        .args(args.iter())
        //.assert()
        .status()
        .unwrap();
    assert!(status.success());
    assert_eq!(true, predicate_file.eval(out_file.as_path()));
}

#[test]
fn test_direct_alt() {
    let args = [
        "--alt",
        "--acc_file",
        "tests/data/acc_with_alt.txt",
        "tests/data/test.fa",
        "--out",
        "out_test_alt.fa",
    ];
    let predicate_file =
        predicates::path::eq_file(Path::new("tests/data/test_direct_alt_outref.fa"))
            .utf8()
            .unwrap();
    let status = escargot::CargoBuild::new()
        .bin("seqbyacc")
        .run()
        .unwrap()
        .command()
        .args(args.iter())
        .status()
        .unwrap();
    assert!(status.success());
    assert_eq!(true, predicate_file.eval(Path::new("out_test_alt.fa")));
}

#[test]
fn test_direct_alt_primary() {
    let args = [
        "--alt_primary",
        "--acc_file",
        "tests/data/acc_with_alt_primary.txt",
        "tests/data/test.fa",
        "--out",
        "out_test_alt_primary.fa",
    ];
    let predicate_file =
        predicates::path::eq_file(Path::new("tests/data/test_direct_alt_primary_outref.fa"))
            .utf8()
            .unwrap();
    let status = escargot::CargoBuild::new()
        .bin("seqbyacc")
        .run()
        .unwrap()
        .command()
        .args(args.iter())
        .status()
        .unwrap();
    assert!(status.success());
    assert_eq!(
        true,
        predicate_file.eval(Path::new("out_test_alt_primary.fa"))
    );
}

#[test]
fn test_yaml() {
    let tmpdir = tempdir().unwrap();
    // eprintln!("{}", tmpdir.path().to_str().unwrap());
    let args = [
        "-c",
        "tests/data/nr-10k_3filters.yaml",
        "tests/data/nr-10k.fa.gz",
        "--dir",
        tmpdir.path().to_str().unwrap(),
    ];
    let outfiles: Vec<String> = ["a", "b", "c"]
        .iter()
        .map(|f| {
            let acc_f = format!("acc_nr-10k-shuff25k{}.txt", *f);
            let org_f = PathBuf::from("tests/data").join(&acc_f);
            let new_f = tmpdir.path().join(&acc_f);
            let _res = std::fs::copy(org_f.as_path(), new_f.as_path());
            let out_f = format!("shuff{}.fa", *f);
            out_f
        })
        .collect();
    let status = escargot::CargoBuild::new()
        .bin("seqbyacc")
        .run()
        .unwrap()
        .command()
        .args(args.iter())
        .status()
        .unwrap();
    assert!(status.success());
    outfiles.iter().for_each(|f| {
        let ref_f = PathBuf::from("tests/data").join(&f);
        let out_f = tmpdir.path().join(f);
        let predicate_file = predicates::path::eq_file(&ref_f);
        assert_eq!(
            true,
            predicate_file.eval(out_f.as_path()),
            "{} == {}",
            &ref_f.to_str().unwrap(),
            &out_f.to_str().unwrap()
        );
    });
}
