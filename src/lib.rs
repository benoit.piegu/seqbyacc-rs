use std::{cell::RefCell, path::PathBuf};

pub mod filters;
pub mod out;

thread_local!(pub static DEBUG: RefCell<u64> = RefCell::new(0));

pub fn type_of<T>(_: &T) -> &str {
    std::any::type_name::<T>()
}

// cf https://stackoverflow.com/questions/54267608/expand-tilde-in-rust-path-idiomatically
pub fn get_path(s: &str) -> PathBuf {
    let mut p = PathBuf::new();
    p.push(shellexpand::tilde(s).as_ref());
    p
}
