/*
 * 2021-07-19
 * Author: Benoît Piégu (CNRS)  <benoit.piegu@cnrs.fr>
 * fastafilter-rs is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * or see the on-line version at http://www.gnu.org/copyleft/gpl.txt
*/

// cf https://rust-lang.github.io/rust-clippy/master/index.html#needless_return
// I prefer explicit return: less rusty but more noticeable
#![allow(clippy::needless_return)]

use anyhow::{Context, Result};
use clap::builder::styling;
use clap::{arg, command, crate_authors, crate_description, crate_version};
use lazy_static::lazy_static;
use needletail::parser::{parse_fastx_file, parse_fastx_stdin};
use seqbyacc::filters::{AccFileFilter, AccFilters};
use seqbyacc::out::{OutType, OUT_DEFAULT_BUFSIZE};
use seqbyacc::{get_path, type_of, DEBUG};
use std::env;
use std::error::Error;
use std::path::PathBuf;

lazy_static! {
    static ref HUMAN_DEFAULT_BUFSIZE: String = format!("{}", OUT_DEFAULT_BUFSIZE);
}

// the template for help
pub const HELP_TEMPLATE: &str = "\
{before-help}{name} (v{version})
{author}

{about-with-newline}
{usage-heading} {usage}

{all-args}{after-help}

";

fn get_humansize(s: &str) -> Result<usize, <usize as std::str::FromStr>::Err> {
    let (is_unit, unit) = match s.chars().last().unwrap() {
        'b' => (true, 1),
        'k' => (true, 1024),
        'M' => (true, 1024 << 10),
        'G' => (true, 1024 << 20),
        _ => (false, 1),
    };
    let mut clean_s = s;
    if is_unit {
        // remove last char
        clean_s = &s[0..s.len() - 1];
    }
    let size = clean_s.parse::<usize>()?;
    Ok(size * unit)
}

fn main() -> Result<(), Box<dyn Error>> {
    let styles = styling::Styles::styled()
        .header(styling::AnsiColor::Green.on_default() | styling::Effects::BOLD)
        .usage(styling::AnsiColor::Green.on_default() | styling::Effects::BOLD)
        .literal(styling::AnsiColor::Blue.on_default() | styling::Effects::BOLD)
        .placeholder(styling::AnsiColor::Cyan.on_default());

    let app = command!()
        .about(crate_description!())
        .author(crate_authors!(", "))
        .styles(styles)
        .help_template(HELP_TEMPLATE)
        .arg_required_else_help(true)
        .version(crate_version!())
        //.author(env!("CARGO_PKG_AUTHORS"))
        .arg(arg!(-d --debug ... "Sets the level of debugging").required(false))
        .arg(
            arg!(-b --bufsize [BUFSIZE]  "Set the size of the output buffers")
                .hide(true)
                // .validator(|s| valid_humansize(s))
                .default_value(&HUMAN_DEFAULT_BUFSIZE[..]),
        )
        .arg(arg!(--dir <DIR> "Set the working directory").required(false))
        .arg(
            arg!(-a --alt "Manage alternatives accessions (accessions in description)")
                .required(false)
                .action(clap::ArgAction::SetTrue),
        )
        .arg(
            arg!(--alt_primary "Can assess alternative accessions as primary accessions")
                .required(false),
        )
        .arg(
            arg!(-c --conf <CONF_FILE> "YAML file configuring multiple filters")
                .required(false)
                .required_unless_present("acc_file"),
        )
        .arg(
            arg!(--acc_file <ACCESSIONS_FILE> "Sets the file of accessions to filter")
                .required(false)
                .required_unless_present("conf"),
        )
        .arg(arg!(-o --out <OUT> "Output file name").required(false))
        .arg(
            arg!(<FASTA> "Sets the fasta file to filter (- for stdin) -- mandatory").required(true),
        );
    let arguments = app.get_matches();
    let fasta_path = match arguments.get_one::<String>("FASTA") {
        Some(file) => file,
        None => return Err("Fasta file is mandatory!".into()),
    };
    let debug = DEBUG.with(|a| {
        *a.borrow_mut() = arguments.get_count("debug") as u64;
        *a.borrow()
    });
    if debug > 1 {
        eprintln!("# arguments={:#?} ({})", arguments, type_of(&arguments));
    }
    let with_alt_acc: bool = arguments.get_flag("alt");
    let with_primary_alt: bool = arguments.get_flag("alt_primary");
    let bufsize = get_humansize(arguments.get_one::<String>("bufsize").unwrap())?;
    if debug > 0 {
        eprintln!("# bufsize={}", bufsize);
    }
    let working_dir: Option<PathBuf> = arguments
        .get_one::<String>("dir")
        //.copied()
        .map(|s| get_path(s));
    let reader = if fasta_path.eq("-") {
        parse_fastx_stdin().with_context(|| "Unable to open stdin as fasta file".to_string())?
    } else {
        parse_fastx_file(fasta_path)
            .with_context(|| format!("Unable to open fasta file (<{}>)", fasta_path))?
    };
    let mut accfilters = AccFilters::new(working_dir, with_alt_acc, with_primary_alt);
    // create and add an AccFilter with
    //   acc_path=accessions_path, out=stdout, log=stderr
    let out_path: OutType<&str> = match arguments.get_one::<String>("out") {
        Some(fout) => OutType::Path(&fout[..]),
        _ => OutType::Stdout,
    };
    if let Some(accessions_path) = arguments.get_one::<String>("acc_file") {
        eprintln!(
            "# fasta file: {}\taccessions file {}",
            fasta_path, accessions_path
        );
        accfilters.add_accfilefilter(AccFileFilter::new(
            &String::from("_"),
            accessions_path,
            out_path,
            OutType::Stderr,
        ))?;
    }
    // configures filters from yml conf file
    if let Some(conf_path) = arguments.get_one::<String>("conf") {
        accfilters.conf_from_yaml_file(conf_path)?;
    }
    if accfilters.is_empty() {
        return Err("Error: There is no filter. Abort".into());
    }
    eprintln!("# {} filter", accfilters.nfilters());
    if debug > 1 {
        eprintln!(" accfilters={:#?}", accfilters);
    }
    accfilters.init_filters(Some(bufsize))?;
    accfilters.seq_filtering(reader)?;
    accfilters.out_stat();
    accfilters.out_acc_not_found()?;
    Ok(())
}
