use std::{
    fmt,
    fs::File,
    io::{self, BufWriter, Write},
    path::PathBuf,
};

pub const OUT_DEFAULT_BUFSIZE: usize = 1024 * 8;

/// Enum describing an output: Stdout, Stderr or file
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum OutType<T> {
    Path(T),
    Stdout,
    Stderr,
}

impl<T: std::fmt::Debug> fmt::Display for OutType<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            OutType::Path(path) => write!(f, "{:?}", path),
            OutType::Stdout => write!(f, "stdout"),
            OutType::Stderr => write!(f, "stderr"),
        }
    }
}

/// Struct managing an buffered output
/// can be either Stdout or Stderr or an opened file
pub struct Out {
    /// write to the provided file, or `stdout` when not provided
    pub out_path: OutType<PathBuf>,
    pub out: Box<dyn Write>,
}

impl Out {
    /// Create an Out struct
    /// Out.out is not open, it'just initialized to io::stdout()
    /// Out.create() must be used to open to the real value (stdout, stderr or file)
    pub fn new<P>(file_out: OutType<P>) -> Out
    where
        PathBuf: From<P>,
    {
        let out_path: OutType<PathBuf> = match file_out {
            OutType::Path(path) => OutType::Path(PathBuf::from(path)),
            OutType::Stdout => OutType::Stdout,
            OutType::Stderr => OutType::Stderr,
        };
        // out start with stdout. out must be with created with method create_output()
        Out {
            out_path,
            out: Box::new(io::stdout()),
        }
    }

    /// create: open Out.out with buffering accordingly to the value of Out.out_path
    /// A capacity can be specified.
    /// If the value is None, a default capacity is configured
    pub fn create_output(&mut self, capacity: Option<usize>) -> std::io::Result<()> {
        //Some(ref path) => File::open(path).map(|f| Box::new(f) as Box<dyn Write>),
        let capacity = match capacity {
            Some(capacity) => capacity,
            None => OUT_DEFAULT_BUFSIZE,
        };
        match &self.out_path {
            //if let OutType::Path(path) = &self.out_path {
            OutType::Path(path) => {
                let new_out = File::create(path); //as Box<dyn Write>;
                match new_out {
                    Ok(out) => {
                        self.out =
                            Box::new(BufWriter::with_capacity(capacity, out)) as Box<dyn Write>
                    }
                    Err(e) => {
                        //drop(new_out);
                        return Err(e);
                    }
                };
            }
            OutType::Stdout => {
                self.out = Box::new(BufWriter::with_capacity(capacity, io::stdout()));
            }
            OutType::Stderr => {
                self.out = Box::new(BufWriter::with_capacity(capacity, io::stderr()));
            }
        }
        Ok(())
    }
}

impl fmt::Debug for Out {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Out")
            .field("out_path", &self.out_path)
            .finish()
    }
}

impl fmt::Display for Out {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Out")
            .field("out_path", &self.out_path)
            .finish()
    }
}
