#![allow(clippy::needless_return)]
use std::{
    collections::HashMap,
    env,
    error::Error,
    fs::{self, File},
    hash::BuildHasherDefault,
    io::BufReader,
    path::PathBuf,
};

use ahash::AHasher;
use anyhow::Context;
use bstr::io::BufReadExt;
use itertools::Itertools;
use memchr::memchr;
use needletail::parser::SequenceRecord;
use yaml_rust::{yaml, Yaml};

use crate::{
    get_path,
    out::{Out, OutType},
    type_of, DEBUG,
};

type IDAccFilter = u16;
#[derive(Debug)]
pub struct AccFileFilter {
    /// name of filter
    name: String,
    /// path to the file containing accessions that need to be found
    acc_path: String,
    /// output for sequences
    out: Out,
    /// output for log
    log: Out,
    /// number of sequences filtered
    n_seq: usize,
    /// number of accessions that need to be found
    n_acc: usize,
    /// number of acc of this filter detected in fasta file as primary accessions
    n_selected_acc: usize,
    /// number of acc of this filter detected in fasta file as alternate accession
    n_alt_selected_acc: usize,
    /// number of acc of this filter detected in fasta file as alternate accession and used as primary accession
    n_alt_as_primary_acc: usize,
}

impl AccFileFilter {
    pub fn new(
        name: &str,
        acc_path: &str,
        out_path: OutType<&str>,
        log_path: OutType<&str>,
    ) -> AccFileFilter {
        AccFileFilter {
            name: name.to_string(),
            acc_path: acc_path.to_string(),
            out: Out::new(out_path),
            log: Out::new(log_path),
            n_seq: 0,
            n_acc: 0,
            n_selected_acc: 0,
            n_alt_selected_acc: 0,
            n_alt_as_primary_acc: 0,
        }
    }

    fn init_out(&mut self, capacity: Option<usize>) -> std::io::Result<bool> {
        self.out.create_output(capacity)?;
        Ok(true)
    }

    fn init_log(&mut self, capacity: Option<usize>) -> std::io::Result<bool> {
        self.log.create_output(capacity)?;
        Ok(true)
    }

    fn from_yaml(yname: &Yaml, yconf: &Yaml) -> Result<AccFileFilter, String> {
        if let Yaml::String(filter_name) = yname {
            let filter_name = filter_name.to_string();
            if yconf["acc"].is_badvalue() {
                return Err(format!(
                    "In filter {}, the <acc> key doesn't exist",
                    filter_name
                ));
            }
            let acc = yconf["acc"].as_str().unwrap().to_string();
            if yconf["out"].is_badvalue() {
                return Err(format!(
                    "In filter {}, the <out> key doesn't exist",
                    filter_name
                ));
            }
            let out = OutType::Path(yconf["out"].as_str().unwrap());
            let log = if yconf["log"].is_badvalue() {
                OutType::Stderr::<&str>
            } else {
                OutType::Path(yconf["log"].as_str().unwrap())
            };
            return Ok(AccFileFilter::new(&filter_name, &acc, out, log));
        } else {
            return Err("Name is not a String".to_string());
        }
    }

    fn read_acc(&mut self, hash_acc: &mut HashAcc, id: IDAccFilter) -> std::io::Result<usize> {
        //let err_msg = format!("Can't open accession file <{}> for filter <{}>", &self.acc_path, &self.name);
        let file = File::open(&self.acc_path)?;
        let buff = BufReader::new(file);
        for line in buff.byte_lines().flatten() {
            // for (_nlines, line) in buff.byte_lines().flatten().enumerate() {
            if line.starts_with(b"#") {
                continue;
            }
            // eprintln!("   {} line={}", _nlines, l);
            //hash_acc.entry(line).or_insert_with(Vec::new).push(self.id);
            hash_acc.entry(line).or_default().push(id);
            self.n_acc += 1;
        }
        Ok(self.n_acc)
    }

    fn stat(&mut self) -> String {
        let n_selected = self.n_selected_acc + self.n_alt_selected_acc + self.n_alt_as_primary_acc;
        let delta_n: i64 = self.n_acc as i64 - n_selected as i64;
        let str_stat= format!(
            "#   * filter <{}> (out={} log={}): {} seq; {} (Δ={}) -- {} acc required; {} filtered ({} + {} alt{})",
            self.name,
            self.out.out_path,
            self.log.out_path,
            self.n_seq,
            if n_selected == self.n_acc {
                "OK"
            } else {
                "problem!"
            },
            delta_n,
            self.n_acc,
            n_selected,
            self.n_selected_acc,
            self.n_alt_selected_acc,
            if self.n_alt_as_primary_acc != 0 {
                format!(" and {} alt as primary", self.n_alt_as_primary_acc)
            } else { String::new() }
        );
        writeln!(self.log.out, "{}", str_stat).unwrap();
        str_stat
    }
}

type HashAcc = HashMap<Vec<u8>, Vec<IDAccFilter>, BuildHasherDefault<AHasher>>;

#[derive(Debug)]
pub struct AccFilters {
    working_dir: Option<PathBuf>,
    with_alt_acc: bool,
    alt_primary: bool,
    accfilters: Vec<AccFileFilter>,
    hash_acc: HashAcc,
    n_tot_acc: usize,
}

impl AccFilters {
    pub fn new(working_dir: Option<PathBuf>, with_alt_acc: bool, alt_primary: bool) -> AccFilters {
        let mut with_alt_acc = with_alt_acc;
        if alt_primary {
            // implied with_alt_acc
            with_alt_acc = true;
        }
        AccFilters {
            working_dir,
            with_alt_acc,
            alt_primary,
            accfilters: Vec::new(),
            hash_acc: HashMap::default(),
            n_tot_acc: 0,
            //hash_filters: HashMap::new(),
        }
    }

    pub fn nfilters(&self) -> usize {
        self.accfilters.len()
    }

    pub fn is_empty(&self) -> bool {
        self.accfilters.len() == 0
    }

    fn nacc(&self) -> usize {
        self.hash_acc.len()
    }

    fn _accfilters_iter(&self) -> std::slice::Iter<AccFileFilter> {
        self.accfilters.iter()
    }

    pub fn add_accfilefilter(&mut self, filter: AccFileFilter) -> Result<(), Box<dyn Error>> {
        self.accfilters.push(filter);
        Ok(())
    }

    pub fn conf_from_yaml_file(&mut self, conf_path: &str) -> Result<(), Box<dyn Error>> {
        let s = fs::read_to_string(conf_path)
            .with_context(|| format!("Error: Unable to read conf file (<{}>)", conf_path))?;
        if let Err(err) = self.conf_from_yaml(&s) {
            Err(format!("  {} from file <{}>", err, conf_path).into())
        } else {
            Ok(())
        }
    }

    fn conf_from_yaml(&mut self, s: &str) -> Result<(), Box<dyn Error>> {
        let config = yaml::YamlLoader::load_from_str(s)?;
        let config = &config[0];
        if let Some(conf_with_alt_acc) = config["with_alt_acc"].as_bool() {
            self.with_alt_acc = conf_with_alt_acc;
        }
        if let Some(conf_alt_primary) = config["alt_primary"].as_bool() {
            self.alt_primary = conf_alt_primary;
            if conf_alt_primary {
                // implied with alt_primary
                self.with_alt_acc = true;
            }
        }
        if let Some(dir) = config["dir"].as_str() {
            self.working_dir = Some(get_path(dir));
        }
        if let yaml_rust::Yaml::Hash(yaml_filters) = &config["filters"] {
            let nerr: u32 = yaml_filters
                .iter()
                .map(|(yname, yconf)| {
                    let res_filter = AccFileFilter::from_yaml(yname, yconf);
                    match res_filter {
                        Ok(filter) => {
                            if let Err(boxed_msg) = self.add_accfilefilter(filter) {
                                eprintln!("{:?}", boxed_msg);
                                // one error
                                1
                            } else {
                                // no error
                                0
                            }
                        }
                        Err(msg) => {
                            eprintln!("  Can't create filter: {}", msg);
                            1
                        }
                    }
                })
                .sum();
            if nerr > 0 {
                return Err(
                    format!("  in yaml configuration file -- {} errors in filter", nerr).into(),
                );
            }
        } else {
            return Err("  the <filters> key doesn't exist".to_string().into());
        }
        Ok(())
    }

    pub fn init_filters(&mut self, capacity: Option<usize>) -> Result<(), Box<dyn Error>> {
        let debug = DEBUG.with(|a| *a.borrow());
        let initial_dir = env::current_dir()?;
        if let Some(dir) = &self.working_dir {
            if debug > 0 {
                eprintln!("# change directory to <{}>", &dir.display());
            }
            env::set_current_dir(dir).with_context(|| {
                format!(
                    "Unable to set working dir to <{}> (from option --dir or conf file)",
                    &dir.display()
                )
            })?;
        }
        if debug > 0 {
            eprintln!("# try opening all files defined in filters");
        }
        let mut nerr: u32 = self
            .accfilters
            .iter_mut()
            .map(|f| {
                let res_out = &f.init_out(capacity);
                match res_out {
                    Ok(_) => 0,
                    Err(why) => {
                        eprintln!(
                            "  Error: Can't initialize out <{}> for filter {}: {}",
                            f.out.out_path, f.name, why
                        );
                        1
                    }
                }
            })
            .sum();
        // open log files
        nerr += self
            .accfilters
            .iter_mut()
            .map(|f| {
                let res_out = &f.init_log(capacity);
                match res_out {
                    Ok(_) => 0,
                    Err(why) => {
                        eprintln!(
                            "  Error: Can't initialize log <{}> for filter {}: {}",
                            f.log.out_path, f.name, why
                        );
                        1
                    }
                }
            })
            .sum::<u32>();
        // read accession files
        //let mut hash_acc: HashMap<Vec<u8>, Vec<usize>> = HashMap::new();
        for (id, f) in self.accfilters.iter_mut().enumerate() {
            let res_out = f.read_acc(&mut self.hash_acc, id as IDAccFilter);
            if let Err(why) = res_out {
                eprintln!(
                    "  Error: Can't read accession file <{}> for filter {}: {}",
                    f.acc_path, f.name, why
                );
                nerr += 1;
            }
        }
        // All files are open. Initial working dir can be restored
        if debug > 0 {
            eprintln!("# restore initial directory (<{}>)", initial_dir.display());
        }
        env::set_current_dir(&initial_dir).with_context(|| {
            format!(
                "Unable to set working dir to initial value <{}>",
                initial_dir.display()
            )
        })?;
        if nerr > 0 {
            return Err(format!("Too many errors ({})", nerr).into());
        } else {
            self.n_tot_acc = self.hash_acc.len();
            //self.hash_acc = Some(hash_acc);
        }
        if debug > 1 {
            //eprintln!("# filters ({}) {}", accfilters.len(), type_of(&accfilters));
            eprintln!(
                "# hash filters ({}) {}\n{:#?}",
                self.accfilters.len(),
                type_of(&self.accfilters),
                &self.accfilters
            );
            eprintln!(
                "# hash_acc={:#?} ({})",
                self.hash_acc,
                type_of(&self.hash_acc)
            );
        }
        Ok(())
    }

    pub fn seq_write(
        &mut self,
        seqrec: &SequenceRecord,
        v_idfilter: &[IDAccFilter],
    ) -> Result<(), Box<dyn Error>> {
        //let res = v_idfilter.iter().try_for_each(|&id|
        let res = v_idfilter
            .iter()
            .enumerate()
            .filter(|(_id, v)| **v != 0)
            .try_for_each(|(id, _v)| {
                let res = seqrec.write(&mut *self.accfilters[id].out.out, None);
                if let Err(why) = res {
                    let s_id = unsafe { std::str::from_utf8_unchecked(seqrec.id()) };
                    return Err(format!(
                        "  Error: write sequence(id={}) for filter {:?} -- {}",
                        s_id, &self.accfilters[id], why
                    )
                    .into());
                }
                self.accfilters[id].n_seq += 1;
                Ok(())
            });
        res
    }

    pub fn seq_filtering(
        &mut self,
        mut seqreader: Box<dyn needletail::FastxReader>,
    ) -> Result<(), Box<dyn Error>> {
        let debug = DEBUG.with(|a| *a.borrow());
        let n_tot_acc: usize = self.nacc();
        let mut n_seq: usize = 0;
        let mut n_seq_filtered: usize = 0;
        //let ln_ending = Some(needletail::parser::LineEnding::Unix);
        // v_idfilter_to_write: vector containing the count of accessions for a each idfilter
        // for the current sequence. Use an u16 for counting considering that there will be
        // no more than 65536 alternate accessions for one sequence
        let mut v_idfilter_to_write: Vec<u16> = vec![0; self.accfilters.len()];
        while let Some(seqrec) = seqreader.next() {
            let seqrec = seqrec.unwrap(); // "invalid record"
            n_seq += 1;
            let seq_header = seqrec.id();
            let pos_end_of_id = memchr(b' ', seq_header).unwrap_or(seq_header.len());
            let id = &seq_header[0..pos_end_of_id];
            if debug > 1 {
                let s_id = unsafe { std::str::from_utf8_unchecked(id) };
                eprintln!(
                    "# id={:?} ({} => {:?})",
                    s_id,
                    type_of(&id),
                    self.hash_acc.get(id)
                );
            }
            // test if accession should be written
            let mut seq_to_write = false;
            // v_idfilter_to_write: vector along idfilter containing count of accessions.
            // v_idfilter_to_write[i] >= 1 means seq to be write for the filter i
            if let Some(v_idfilter) = self.hash_acc.remove(id) {
                seq_to_write = true;
                // init v_idfilter_to_write only if necessary
                v_idfilter_to_write.iter_mut().for_each(|c| *c = 0);
                v_idfilter.iter().for_each(|&id_filter| {
                    //* hash_idfilter_to_write.insert(id_filter, 0);
                    v_idfilter_to_write[id_filter as usize] = 1;
                    self.accfilters[id_filter as usize].n_selected_acc += 1
                });
            }
            // search for alternate accessions
            /*  example:
            >Q57293.1 RecName: Full=Fe(3+) ions import ATP-binding protein FbpC ^AAAB05030.1 afuC
            [Actinobacillus pleuropneumoniae] ^AAAB17216.1 afuC [Actinobacillus pleuropneumoniae]
            */
            if ((self.with_alt_acc && seq_to_write) || self.alt_primary)
                && pos_end_of_id + 1 < seq_header.len()
            {
                // init v_idfilter_to_write only if necessary
                if !seq_to_write {
                    v_idfilter_to_write.iter_mut().for_each(|c| *c = 0);
                }
                let desc = &seq_header[pos_end_of_id + 1..];
                // get all alternate accessions from desc
                let alt_acc_vec: Vec<&[u8]> = desc
                    // split desc with ^A as delimiter to get alternative accessions
                    .split(|c| *c == 1)
                    .map(|sub| {
                        if let Some((id, _desc)) = sub
                            // split on space to get id and desc
                            .splitn(2, |c| *c == b' ')
                            .collect_tuple()
                        {
                            Some(id)
                        } else {
                            None
                        }
                    })
                    // .filter_map(| id| id)
                    .flatten()
                    .collect();
                if debug > 2 {
                    let alt_acc_str_vec: Vec<&str> = alt_acc_vec
                        .iter()
                        .map(|acc_u8| unsafe { std::str::from_utf8_unchecked(acc_u8) })
                        .collect();
                    eprintln!(
                        "#    alt_acc={:?} {}",
                        alt_acc_str_vec,
                        type_of(&alt_acc_vec)
                    );
                }
                // get all filters for all alternate accessions
                alt_acc_vec.iter().for_each(|acc| {
                    if let Some(v_idfilter) = self.hash_acc.remove(*acc) {
                        v_idfilter.iter().for_each(|&idfilter| {
                            if self.alt_primary && v_idfilter_to_write[idfilter as usize] == 0 {
                                seq_to_write = true;
                                self.accfilters[idfilter as usize].n_alt_as_primary_acc += 1;
                            }
                            v_idfilter_to_write[idfilter as usize] += 1;
                        });
                    }
                });
                if debug > 2 {
                    eprintln!("#     id filter to write={:?}", v_idfilter_to_write);
                }
                v_idfilter_to_write
                    .iter()
                    .enumerate()
                    .for_each(|(idfilter, n)| {
                        if *n > 0 {
                            // count is the number of alternate accession
                            // +1 (the primary accession)
                            self.accfilters[idfilter].n_alt_selected_acc += *n as usize - 1;
                        }
                    });
            }
            if seq_to_write {
                // the sequence must to be write for all filters
                self.seq_write(&seqrec, &v_idfilter_to_write)?;
            }
            n_seq_filtered += seq_to_write as usize;
        }
        if debug > 0 {
            eprintln!(
                "# {} sequences; {} sequences filtered for {} accessions",
                n_seq, n_seq_filtered, n_tot_acc
            );
        }
        Ok(())
    }

    pub fn out_stat(&mut self) {
        self.accfilters.iter_mut().for_each(|filter| {
            let str_stat = filter.stat();
            if filter.log.out_path != OutType::Stderr {
                eprintln!("{}", str_stat);
            }
        });
    }
    pub fn out_acc_not_found(&mut self) -> Result<(), Box<dyn Error>> {
        for (acc, v_idfilter) in self.hash_acc.iter() {
            let s_acc = unsafe { std::str::from_utf8_unchecked(&acc[..]) };
            for id in v_idfilter.iter() {
                writeln!(&mut *self.accfilters[*id as usize].log.out, "{}", s_acc)?;
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use tempfile::tempdir;
    #[test]
    fn test_out() {
        let tmpdir = tempdir().unwrap();
        let file_path = tmpdir.path().join("foo.txt");
        let out = Out::new(OutType::Path(file_path.to_str().unwrap()));
        assert!(matches!(&out.out_path, OutType::Path(p) if *p == file_path));
        /* if let OutType::Path(p) = out.out_path {
            assert_eq!(p, file_path);
        } else {
            assert!(false);
        } */
    }

    #[test]
    fn test_yaml() {
        let yaml_str = r#"dir: tests/
alt_primary: true
filters:
    shuffa:
        acc: acc_nr-10k-shuff25ka.txt
        out: shuffa.fa
        log: shuffa.log
    shuffb:
        acc: acc_nr-10k-shuff25kb.txt
        out: shuffb.fa
        log: shuffb.log
"#;
        let mut accfilters = AccFilters::new(Some(PathBuf::from(".")), true, true);
        let _res = accfilters.conf_from_yaml(&yaml_str);
        if let Ok(res) = _res {
            assert_eq!(res, ());
            assert_eq!(accfilters.alt_primary, true);
            assert_eq!(accfilters.nfilters(), 2);
            let filter = accfilters._accfilters_iter().next().unwrap();
            assert_eq!(filter.name, "shuffa");
            assert_eq!(filter.acc_path, "acc_nr-10k-shuff25ka.txt");
            assert!(
                matches!(&filter.out.out_path, OutType::Path(p) if *p == PathBuf::from("shuffa.fa"))
            );
            assert!(
                matches!(&filter.log.out_path, OutType::Path(p) if *p == PathBuf::from("shuffa.log"))
            );
        } else {
            assert!(false);
        }
    }
}
